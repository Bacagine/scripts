#!/bin/bash

sudo apt install linux-source
sudo apt install glibc-source
sudo ln -s /usr/src/linux-* /usr/src/linux
cd /usr/src/glibc/
tar -xvf glibc-*.tar.gz
sudo mkdir /usr/src/glibc-build
cd /usr/src/linux/
sudo make mrproper
sudo make INSTALL_HDR_PATH=dest headers_install
sudo find dest/include \( -name .install -o -name ..install.cmd \) -delete
sudo cp dest/include/* /usr/include
cd /usr/src/glibc/glibc-build/
sudo CC="gcc -isystem /usr/lib/gcc/x86_64-linux-gnu/8/include -isystem /usr/include" \
../glibc-*.*/configure --prefix=/usr \
--disable-werror \
--enable-kernel=4.19.0-9 \
--enable-stack-protector=strong \
libc_cv_slibdir=/lib
sudo make
sudo make check
sudo make install
