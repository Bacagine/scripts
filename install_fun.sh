#!/bin/bash

# Install cmatrix
git clone https://github.com/abishekvashok/cmatrix.git
cd cmatrix
autoreconf -i
mkdir build && cd build
../configure && make && make install

cd ../../

# Install CTris
git clone https://github.com/MichaelPaulin/CTris.git
cd CTris make && make install

# Install Glava
git clone https://github.com/jarcode-foss/glava
cd glava
meson build --prefix /usr
ninja -C build
sudo ninja -C build install

